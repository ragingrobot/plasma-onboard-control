# Plasma Onboard Control

## What is it?

A small, nearly featureless plasmoid, whose sole function is to toggle the Onboard virtual keyboard. It uses the DBus interface to do so.

## Requirements

The following development packages are needed:

  - KF5I18n (translations welcome)
  - KF5Plasma
  - Qt5QDBus
  - Qt5Qml
