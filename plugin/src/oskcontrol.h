/*
    SPDX-FileCopyrightText: 2021 Anthony Thomasel anthony.thomasel@gmail.com
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#ifndef OSKCONTROL_H
#define OSKCONTROL_H

#include "onboard_proxy.h"

#include <QObject>
#include <QDBusAbstractInterface>
#include <QSharedPointer>

class OSKControl : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool keyboardVisible READ isVisible WRITE setVisible)
  
public:
  explicit OSKControl(QObject *parent = nullptr);
  ~OSKControl();
  
  bool isVisible() const;
  void setVisible(const bool);

  Q_INVOKABLE void toggleVisible();
  
private:
  bool keyboard_visible;
  QSharedPointer<org::onboard::Onboard::Keyboard> dbus_control;

  void createDBusControl();
};

#endif
