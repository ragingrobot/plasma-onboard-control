/*
  SPDX-FileCopyrightText: 2021 Anthony Thomasel anthony.thomasel@gmail.com
  SPDX-License-Identifier: LGPL-2.1-or-later
*/

#ifndef OSKCONTROLPLUGIN_H
#define OSKCONTROLPLUGIN_H

#include <QObject>
#include <QQmlExtensionPlugin>

class OSKControlPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")
  
public:
  void registerTypes(const char *uri) override;
};

#endif
