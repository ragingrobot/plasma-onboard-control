/*
    SPDX-FileCopyrightText: 2021 Anthony Thomasel anthonyDOTthomasel@gmail-dot-com
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "oskcontrol.h"
#include "oskcontrol_plugin.h"

#include <QQmlEngine>

void OSKControlPlugin::registerTypes(const char* uri)
{
    Q_ASSERT(uri == QLatin1String("org.kde.plasma.private.oskcontrol"));
    qmlRegisterType<OSKControl>(uri, 0, 1, "OSKControl");
}
