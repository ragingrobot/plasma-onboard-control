/*
    SPDX-FileCopyrightText: 2021 Anthony Thomasel anthonyDOTthomasel@gmail-dot-com
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "oskcontrol.h"
#include "onboard_proxy.h"

#include <KLocalizedString>
#include <QDBusAbstractInterface>
#include <QtWidgets/QMessageBox>

OSKControl::OSKControl(QObject *parent)
  : QObject(parent),
    keyboard_visible(false)
{
  this->createDBusControl();
}

OSKControl::~OSKControl()
{
}

bool OSKControl::isVisible() const
{
  return dbus_control->visible();
}


void OSKControl::setVisible(const bool value)
{
  keyboard_visible = value;
  this->toggleVisible();
}

void OSKControl::toggleVisible()
{
  QMessageBox::StandardButton reply;
  dbus_control->ToggleVisible();
}

void OSKControl::createDBusControl()
{
  dbus_control =
    QSharedPointer<org::onboard::Onboard::Keyboard>(new org::onboard::Onboard::Keyboard("org.onboard.Onboard", "/org/onboard/Onboard/Keyboard", QDBusConnection::sessionBus(), this));
}
