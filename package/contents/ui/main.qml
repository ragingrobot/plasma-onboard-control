/*
  SPDX-FileCopyrightText: 2021 Anthony Thomasel <anthony.thomasel@gmail.com>

  SPDX-License-Identifier: LGPL-2.0-or-later
*/

import QtQuick 2.3
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0 as QtControls

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0

import org.kde.plasma.private.oskcontrol 0.1

import QtQuick.Dialogs 1.1

QtObject {
    id: vkcontrol

    property QtObject keyboard: OSKControl { }

    readonly property bool horizontal: plasmoid.formFactor === PlasmaCore.Types.Horizontal
    readonly property int preferredLength: units.smallSpacing * 2

    Layout.fillWidth: !horiztonal
    Layout.fillHeight: horiztonal

    Layout.minimumWidth: horizontal ? preferredLength : -1
    Layout.preferredWidth: Layout.minimumWidth
    Layout.maximumWidth: Layout.minimumWidth

    Layout.minimumHeight: !horizontal ? units.iconSizes.small : -1
    Layout.preferredHeight: Layout.minimumHeight
    Layout.maximumHeight: Layout.minimumHeight

    Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation
    Plasmoid.backgroundHints: PlasmaCore.Types.NoBackground

    Plasmoid.onActivated: activate
    Plasmoid.icon: plasmoid.configuration.icon
    Plasmoid.title: i18n("Virtual Keyboard")
    Plasmoid.toolTipSubText: i18n("Control the Virtual Keyboard. Click to toggle display.")

    Plasmoid.fullRepresentation: PlasmaCore.ToolTipArea {

        mainText: plasmoid.title
        subText: plasmoid.toolTipSubText

        MouseArea {
            anchors.fill: parent
            onClicked: action_toggle()
        }

        PlasmaCore.IconItem {
            anchors.fill: parent
            source: plasmoid.icon
        }
    }

    Component.onCompleted: {
        plasmoid.setAction("config", i18nc("@action", "Configure " + Plasmoid.title))
        plasmoid.setAction("quit", i18nc("@action", "Quit " + Plasmoid.title))
    }
    
    function activate() {
    	keyboard.activate()
    }
    
    function action_show() {
	keyboard.show()
    }

    function action_hide() {
	keyboard.hide()
    }

    function action_toggle() {
	keyboard.toggleVisible()
    }

    function action_config() {
    }

    function action_quit()
    {
    }
}
